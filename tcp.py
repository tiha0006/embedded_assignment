# Description:
# TCPConnection handles the server and client side tcp connectivity
# There is a send queue and a receive queue which messages are sent to and processed from
# A callback is accepted on initialisation that translates the tcp stream into a data structure, this is a custom protocol
# Processed data is returned to be stored in the recvqueue
# Server and client specific functions are seperated, and data transmission functions for sending and receiving data are common to both

import socket
#import _thread
import time
#import random
import threading

class TCPConnection():
    def __init__(self, parsefunction):
        self.parsefunction = parsefunction
        self.readcount = 8 #chars read per recv
        self.lock = threading.Lock()

    sendqueue = []
    def enqueue_send(self, item):
        self.sendqueue += [item]

    def send(self):
        if(len(self.sendqueue) == 0): return
        print("sending",self.sendqueue)
        outstring = "tim:"+str(time.time()) + " "
        while len(self.sendqueue) > 0:
            #print ("xx ", outstring)
            outstring += self.sendqueue[0] + " "
            self.sendqueue = self.sendqueue[1:]

        self.sendqueue = []
        with self.lock: self.consock.sendall((outstring).encode('utf-8'))

    recvqueue = []

    def clear_received():
        recvqueue = []

    def read_next(self):
        try:
            return self.recvqueue.pop(0)
        except IndexError:
            return None

    ##############
    ### SERVER ###
    ##############
    def start_server(self, port):
        self.consock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.consock.settimeout(0.00001)
        self.consock.bind(('', port))
        self.consock.listen()

        conspawner = threading.Thread(target=(self.conspawner),args=((self.consock,)))
        conspawner.setDaemon(True)
        conspawner.start()

    openconnections = []
    def conspawner(self, sock):
        while True:
            try:
                print('Ready to receive connection')
                while True:
                    try:
                        consock, address = sock.accept()
                        break
                    except socket.timeout:
                        pass

                print('Connected client at:', address)
            except BaseException as e:
                print("Unanticipated exception.", e)
                raise
            t = threading.Thread(target=(self.conhandler),args=((consock,)))
#            for i in range(len(self.openconnections)):
#                with self.lock:
#                    if not self.openconnections[i].isAlive(): del self.openconnections[i]
            self.openconnections.append(t)
            self.consock = consock #hack?
            t.start()

    def conhandler(self, consock):
        print("handling new connection")
        while True:
            try:
                data_in = ""
                while True:
                    #print(threading.current_thread())
                    singleread = ""
                    try:
                        #print(threading.enumerate())
                        with self.lock: singleread = consock.recv(self.readcount).decode('utf-8')
                        #print(singleread)
                    except socket.timeout:
                        time.sleep(0.3)
                    data_in += singleread
                    if (len(singleread) < self.readcount):
                        break

                if(not self.parse(data_in)):
                    break

            except ConnectionResetError:
                print("Connection reset remotely!")
                break
        #consock.close()
        print('Connection closed')

    def parse(self, s):
        keepalive, l = self.parsefunction(s)
        for i in l:
            print("x",i)
            self.recvqueue.append(i)
        return keepalive

    ##############
    ### CLIENT ###
    ##############
    def connect(self, host, port):
        self.consock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.consock.settimeout(0.00001)
        self.host = host
        self.port = port
        try:
            self.consock.connect((self.host, self.port))
            t = threading.Thread(target=(self.conhandler),args=((self.consock,)))
            t.start()
            print("Connected!")
            return True
        except ConnectionRefusedError:
            print("Connection Refused")
        except socket.timeout as e:
            print("Timeout...")
        except BaseException as e:
            print("Unanticipated exception.", e)
            raise

        return False

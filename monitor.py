# Description:
# Monitor objects are designed to run as seperate threads to asynchronously read and send data
# A read function and a write function are given as callbacks, which are called at a sepcified interval
# Write function is optional
import time
import threading
class Monitor(threading.Thread):
    def __init__(self, readfunction, writefunction, interval):
        threading.Thread.__init__(self)#target=self.run, args=args)
        self.writefunction = writefunction
        self.readfunction = readfunction
        self.interval = interval
        self.setDaemon(True)
        self.start()

    def __init__(self, callback, interval):
        threading.Thread.__init__(self)#target=self.run, args=args)
        self.writefunction = None
        self.readfunction = callback
        self.interval = interval
        self.setDaemon(True)
        self.start()

    def run(self):
        lastread = time.time()
        while True:
            data = self.readfunction() #read
            if self.writefunction: self.writefunction(data) #write
            lastread += self.interval
            time.sleep(max(0, lastread - time.time())) # ensures regular interval



def GPIOIN(x):
    GPIOREADCOUNTER = 1
#    print("reading pin", x, "as", GPIOREADCOUNTER)
    return GPIOREADCOUNTER

def GPIOOUT(x,v):
#    print("set", x, "to", v)
    pass
    
def GPIOINITIALISE(inputs = [], outputs = []):
    #initialise
    #GPIO.setmode(GPIO.BOARD)

    #init inputs
#    print(inputs, "as inputs")

    #init outputs
#    print(outputs, "as outputs")
    pass
def GPIOCLEANUP():
#    print("GPIO.cleanup()")
    pass



def testinputs(inputs,mode = 1):
    if mode == 0:
        for i in inputs:
            print(i, ":", GPIOIN(i))
    elif mode == 1:
        s = "["
        for i in inputs:
            s += " " if GPIOIN(i) else "#"
        s+= "]"
        print(s)
    else:
        print("unknown mode")

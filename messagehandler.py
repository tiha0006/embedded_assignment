# Description:
# the MessageHandler class provides a place for centralising the processing of received messages
# via callbacks that can be registered with it

from messageprotocol import *
import socket
class MessageHandler():
    def __init__(self, connection):
        self.connection = connection

    handlers = {}
    def add_handler(self, messagetoken, handlerfunction):
        self.handlers[messagetoken] = handlerfunction

    def update(self):
        while len(self.connection.recvqueue) != 0:
            item = self.connection.read_next()
            if item[0] in self.handlers:
                self.handlers[item[0]](*(item[1]).split(","))

    def constructandsend(self,token,args):
        m = constructmessage(token, args)
        self.connection.enqueue_send(m)

        try:
            self.connection.send()
        except socket.timeout as e:
            print("Timeout...(not connected?)")

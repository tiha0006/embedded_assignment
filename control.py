# Description:
# The main file, handles the overall flow of the program, handles command lines and
# initialises the necessary variables before entering the main loop
import threading
from pilotcam import *
from gpiowrapper import *
import voltagereader
from cl import *
from tcp import *
from messagehandler import *
import messageprotocol
from brain import *

###  COMMAND LINES VALUE SETTING ###
cl = parseargv()
#print(cl, "-lc" in cl)
if "-cr" in cl: car.changerate = float(cl["-cr"])
if "-s" in cl: car.speed = float(cl["-s"])
####################################

def init_car(brain):
    #initialise
    #GPIO.setmode(GPIO.BOARD)

    #set constant pins
    hi = [16,23]
    GPIOINITIALISE(outputs=hi)
    for i in hi:
    #    GPIO.setup(i, GPIO.OUT)
        GPIOOUT(i, True)
    #lo = []
    #for i in lo:
    #    GPIO.setup(i, GPIO.OUT)
    #    GPIOOUT(i, False)


    #set inputs
    inputs = [21,18,19]#[0,1,2]
    #for i in inputs:
    #    GPIO.setup(i, GPIO.IN)

    #set outputs
    outputs = [31,29,32,15,11,13,40,37,38,36,35,33]
    #outputs = [40,38,37,36,35,33,32,31,29,16,15,13]
    #for i in outputs:
    #    GPIO.setup(i, GPIO.OUT)
    GPIOINITIALISE(inputs, outputs)


    #initialise objects
    tuning = [0.0,1.0]*4
    wheels = [0]*4

    for i in range(len(wheels)):
        wheels[i] = Wheel(outputs[i*3:i*3+3], tuning[2*i:2*i+2])


    return Car(wheels,inputs,brain,.01)



try:

    mh = brain = None

    if "-rc" in cl: ##remote controlled
        if not "-ip" in cl:
            raise Exception("ip needed for remote controller")

        tcpc = TCPConnection(messageprotocol.parsedata)
        while not tcpc.connect(cl["-ip"][0], 65432):
            time.sleep(1)
        mh = MessageHandler(tcpc)

        if "-telem" in cl:
            monitor.Monitor(tcps.send(),1)

        brain = RemoteCamBrain(mh)


    elif "-lc" in cl:   #local controlled
        if "-t" in cl:
            for o in outputs:
                GPIOOUT(o,0)
            try:
                car.testwheels()
            except KeyboardInterrupt:
                for w in car.wheels:
                    w.brake()
            try:
                print("Turn Test")
                car.setwheels(0,1)
                for w in car.wheels:
                    w.pulse()
                while True: pass
            except KeyboardInterrupt:
                pass

        if "-telem" in cl:
            tcpc = TCPConnection(messageprotocol.parsedata)
            tcpc.connect(cl["-ip"][0], 65432)
            monitor.Monitor(tcpc.send(),1)
            #monitor

        brain = CamBrain()

    else:
        print ("choose local or remote (-rc or -lc)")
        raise Exception("QUIT")
    car = init_car(brain)
    ##main loop
    while True:
        if mh: mh.update()
        car.update()
        time.sleep(0.1)
    #while True:
    #    #read buffer
    #    if "telem" in cl:
    #        for m in tcps.read_recieved():
    #            print (m)

    #    car.update()

except Exception as e:
    print (e)
    print ("Shutting down")
    # release pins
    # for o in outputs:
    #     GPIOOUT(o,False)
    GPIOCLEANUP()
    raise

# Description:
# A simple script to process car input remotely and send back commands

from messagehandler import *
import messageprotocol
from tcp import *
from brain import *
import time

brain = CamBrain()

tcps = TCPConnection(messageprotocol.parsedata)
tcps.start_server(65432)
print("Waiting for connection...")
mh = MessageHandler(tcps)

def handler_cse(seeline, cx):
    print("CSE received:", seeline, cx)
    speed, turn = brain.decide(bool(seeline), int(cx))
    mh.constructandsend("cgo",[str(speed),str(turn)])

mh.add_handler("cse", handler_cse)

while True:
    mh.update()
    time.sleep(0.1)

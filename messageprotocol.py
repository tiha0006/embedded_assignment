# Description:
# this function is called to process the recived tcp stream into a more easily interpreted data structure
def parsedata(s):
    s=s.strip()
    if s == "": return True, []
    keepalive = True
    out = []
    print ("rawdata: ", s)
    inputs = s.split(" ")
    for entry in inputs:
        entry = entry.split(":")
        if(len(entry) != 2):    #invalid format
            print ("invalid input \"", entry, "\"")
            continue

        out.append([entry[0], entry[1]])
        if entry[0] == "ins" and entry[1] == "quit": keepalive = False
    print("parsed data", out)
    return keepalive, out

def constructmessage(token,args):
    return token + ":" + ",".join(args)

# Description:
# encapsulates the gpio interface

from gpiowrapperdummy import *
# import RPi.GPIO as GPIO
#
# def GPIOIN(x):
#     v = GPIO.input(x)
#     return v
#
# def GPIOOUT(x,v):
#     GPIO.output(x, v)
#
# def GPIOINITIALISE(inputs = [], outputs = []):
#     #initialise
#     GPIO.setmode(GPIO.BOARD)
#
#     #init inputs
#     for i in inputs:
#         GPIO.setup(i, GPIO.IN)
#
#     #init outputs
#     for i in outputs:
#         GPIO.setup(i, GPIO.OUT)
#
# def GPIOCLEANUP():
#     GPIO.cleanup()
#
# def testinputs(inputs,mode = 1):
#     if mode == 0:
#         for i in inputs:
#             print(i, ":", GPIOIN(i))
#     elif mode == 1:
#         s = "["
#         for i in inputs:
#             s += " " if GPIOIN(i) else "#"
#         s+= "]"
#         print(s)
#     else:
#         print("unknown mode")

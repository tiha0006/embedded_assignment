# Description:
# CamBrain serves as the local decision making class
# RemoteCamBrain reads and returns the same data, but sends and receives that data for remote processing
class Brain():
    def __init__(self):
        pass

    def update(self):
        return -1,-1

import cameranav
class CamBrain(Brain):
    def __init__(self):
        pass

    minval = -80
    maxval = 80
    def decide(self, seeline, rawval):
        print("raw",  rawval, self.minval, self.maxval, rawval/self.minval, rawval/self.maxval)
        self.minval = rawval if rawval < self.minval else self.minval
        self.maxval = rawval if rawval > self.maxval else self.maxval
        if(rawval > 0): rawval /= float(self.maxval)
        if(rawval < 0): rawval /= float(abs(self.minval))
        if seeline:
            return 1, rawval
            #self.setwheels(1,rawval)
        else:
            return 0, rawval
            #self.setwheels(0,rawval)

class RemoteCamBrain(Brain):
    def __init__(self,messagehandler):
        self.messagehandler = messagehandler
        self.turn = 0
        self.speed = 0
        messagehandler.add_handler("cgo", self.handler_cgo)  #does this handle self?

    def handler_cgo(self, speed, turn):
        print("CGO Received!", speed, turn)
        self.speed = float(speed)
        self.turn = float(turn)

    def decide(self, seeline, rawval):
        #send info
        # s = "cse:"+str(seeline)+","+str(rawval)
        # connection.enqueue_send(s)
        # connection.send()
        self.messagehandler.constructandsend("cse",[str(seeline),str(rawval)])

        #recv instructions
        return self.speed, self.turn

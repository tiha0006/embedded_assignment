# Description:
# This module processes the camera image, returning an x value for the line position
# as well as a bool representing whether the line is being seen currently
import numpy as np
import cv2
class CamNav:
    def __init__(self):
        self.video_capture = cv2.VideoCapture(-1)
        self.video_capture.set(3, 160)
        self.video_capture.set(4, 120)

    lp = 0
    def get_position(self): #return x position (cx)
        ret, frame = self.video_capture.read()
        if not ret: return False,0
        crop_img = frame[60:120, 0:160]
        gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray,(5,5),0)
        ret,thresh1 = cv2.threshold(blur,70,255,cv2.THRESH_BINARY_INV)
        mask = cv2.erode(thresh1, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        contours,hierarchy = cv2.findContours(mask.copy(), 1, cv2.CHAIN_APPROX_NONE)
        if len(contours) > 0:
            c = max(contours, key=cv2.contourArea)
            M = cv2.moments(c)

            if M['m00'] == 0:
                cx = camw//2
        #        cy = camh//2
            else:
                cx = int(M['m10']/M['m00'])
        #        cy = int(M['m01']/M['m00'])
        #        cv2.line(crop_img,(cx,0),(cx,720),(255,0,0),1)
        #        cv2.line(crop_img,(0,cy),(1280,cy),(255,0,0),1)
        #        cv2.drawContours(crop_img, contours, -1, (0,255,0), 1)
	    #cv2.imshow('frame',crop_img)
            self.lp=cx-80
            return True, self.lp
        return False, 80 if self.lp > 0 else -80

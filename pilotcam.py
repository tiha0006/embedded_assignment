# Description:
# The code to control the car, taking input and controling the wheels
import brain
from cameranav import *
from gpiowrapper import *
import time
import random

random.seed()

class Wheel:
    def __init__(self, pins, limits):
        print(pins,limits)
        self.pins = pins
        self.speedmin = limits[0]
        self.speedmax = limits[1]

    speedmax = 1.0
    speedmin = 0.0

    def power(self, power): # +1 -1
        if power == 0:
            self.brake()
            return

        #set percentage power 0-1
        self.pp = abs(power)
        #set forwards/backwards
        self.turn(True if power > 0 else False)

    def turn(self, dir):
        GPIOOUT(self.pins[1], dir)
        GPIOOUT(self.pins[2], not dir)

    def brake(self):
        GPIOOUT(self.pins[1], 1)#GPIO.HIGH)
        GPIOOUT(self.pins[2], 1)#GPIO.HIGH)

    pp = 1.0
    def pulse(self,tip = 0): #normalised tip 0-1 and pp is 0-1 percent power
        GPIOOUT(self.pins[0], (self.speedmax - self.speedmin) * self.pp + self.speedmin > tip)

class Sensors:
    def __init__(self, sensors, stime):
        self.stime = stime
        self.spins = sensors

    laststate = 0
    stime = 0
    def read(self, t):
        state = 0
        for s in self.spins:
            state = (state << 1) | (not GPIOIN(s))

        if state != self.laststate:
            self.stime = t

        self.laststate = state

        return state, t - self.stime




class Car:
    def __init__(self, wheels, sensors, brain, interval = 1.0):
        self.pstart = time.time()
        self.time = self.pstart
        self.wheels = wheels
        self.sensors = Sensors(sensors, self.pstart)
        self.interval = interval
        self.brain = brain #navigation object returns turn and speed from update function
        self.cam = CamNav()




    speed = 1.0
    shallowturnlimit = 1.0
    hardturnreverse = 0.0
    minval = -80
    maxval = 80

    def update(self):
        t = time.time()
        self.dt = t - self.time
        self.time = t


        self.setwheels(*(self.brain.decide(*(self.cam.get_position()))))

        #time interval percentage
        tip = ((self.time - self.pstart) % self.interval)/self.interval

        #update wheels
        for w in self.wheels:
           w.pulse(tip)

        #time.sleep(0.001)


    def setwheels(self, linear, turn):
        #print(linear, turn)
        scale = abs(linear) + abs(turn)
        scale = 1 if scale < 1 else scale
        if scale == 0.0:
            self.wheels[0].power(0)    #fl
            self.wheels[1].power(0)    #fr
            self.wheels[2].power(0)    #bl
            self.wheels[3].power(0)    #br
        else:
            self.wheels[0].power((linear + turn)/scale)    #fl
            self.wheels[1].power((linear - turn)/scale)    #fr
            self.wheels[2].power((linear + turn)/scale)    #bl
            self.wheels[3].power((linear - turn)/scale)    #br
        return 0


    def testwheels(self):
        print ("Testing wheels!")
        for i in range(4):
            print (i)
            stime = time.time()
            t = stime
            p = 6
            while t-stime < p:
                t = time.time()
                self.wheels[i].power((t-stime)*2/p -1)
                self.wheels[i].pulse( ((t-stime)%0.001)/0.001 )
                time.sleep(0.001)
            self.wheels[i].power(0)
        print ("Done testin'!")
